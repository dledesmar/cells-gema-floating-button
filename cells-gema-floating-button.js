{
  const {
    html,
  } = Polymer;
  /**
    `<cells-gema-floating-button>` Description.

    Example:

    ```html
    <cells-gema-floating-button
    label="Simular préstamo" 
    information="[[data]]"
    class="secondary size-m"
    ></cells-gema-floating-button>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Default value |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    | --cells-gema-floating-button-position-x | | left | 50% |
    | --cells-gema-floating-button-position-y | | top | 100% |
    | --cells-gema-floating-button-position-x , --cells-gema-floating-button-position-y| | transform: translate | (-50% , -100%) |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-gema-floating-button | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsGemaFloatingButton extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-gema-floating-button';
    }

    static get properties() {
      return {
        /**
         * Buttom name.
         * @type {String}
         */
        label: {
          type: String
        },
        /**
         * Data sended.
         * @type {Object}
         */
        information: {
          type: Object,
          value: () => {}
        },
        /**
         * Access to styles of cells-st-button.
         * @type {String}
         */
        class: {
          type: String
        }
      };
    }

    static get template() {
      return html `
      <style include="cells-gema-floating-button-shared-styles cells-gema-floating-button-styles"></style>
      <slot></slot>
      <div class="container");">
       <cells-st-button class$="[[class]]">
         <button on-click="_handlerEvent" data-information$="[[information]]">{{label}}</button>
       </cells-st-button>
      </div>
       `;
    }

    /**
     * Fires a custom event when an action is triggered
     * @param {Event} e The object event captured
     */

    _handlerEvent(e) {
      //e.preventDefault();
      this.dispatchEvent(new CustomEvent(`${CellsGemaFloatingButton.is}-click`, {
        bubbles: true,
        composed: true,
        detail: e.target.dataset.information
      }));
    }

  }

  customElements.define(CellsGemaFloatingButton.is, CellsGemaFloatingButton);
}